
#ifndef libpcl_EXPORT_H
#define libpcl_EXPORT_H

#ifdef libpcl_BUILT_AS_STATIC
#  define libpcl_EXPORT
#  define LIBPCL_NO_EXPORT
#else
#  ifndef libpcl_EXPORT
#    ifdef libpcl_EXPORTS
        /* We are building this library */
#      define libpcl_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define libpcl_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef LIBPCL_NO_EXPORT
#    define LIBPCL_NO_EXPORT 
#  endif
#endif

#ifndef LIBPCL_DEPRECATED
#  define LIBPCL_DEPRECATED __declspec(deprecated)
#  define LIBPCL_DEPRECATED_EXPORT libpcl_EXPORT __declspec(deprecated)
#  define LIBPCL_DEPRECATED_NO_EXPORT LIBPCL_NO_EXPORT __declspec(deprecated)
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define LIBPCL_NO_DEPRECATED
#endif

#endif
